'use strict';

var request = require('request-promise');
var _ = require('lodash')
var config = require('../config')
var reportGenerator = require('../utils/reportGenerator')


exports.handler = (event, context, callback) => {

    //Validate Envrioment
    var KFPH_ENV = config.get(process.env.ENVIRONMENT);

    if(_.isNil(KFPH_ENV)){
        console.error('Invalid Environment');
        callback(new Error("Fatal Error : Invalid Environment"));
        return;
    }
    console.log("Environment: " + process.env.ENVIRONMENT)
    console.log(JSON.stringify(KFPH_ENV))

    var data = {};

    console.info("Calling the User API to retrieve data");
    request({
        uri: 'https://jsonplaceholder.typicode.com/users',
        method: 'GET',
        resolveWithFullResponse: true 
    })
    .then(function (response) {
        console.info("Retreived data from User API. statusCode" + response.statusCode)
        data = {
        "instrument":{
            "reportVersion": "1.0.0",
            "ReportDefCode": "sandbox.ph.peopleprofile.pdf"
            },
            "data":{
                "people": JSON.parse(response.body)
            }
        }
        return reportGenerator.downloadReport(KFPH_ENV, data, 'sample.pdf')

    })
    .then(function (response) {
        callback(null, response)
        context.done()
    })
    .catch(function (err) {
        callback("Fatal Error :" + JSON.stringify(err));
    });

}