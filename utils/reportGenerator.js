
var AWS = require('aws-sdk');
var request = require('request-promise');
var Guid = require('guid')
var _ = require('lodash')


exports.downloadReport = (envConfig, reportData, filename) => {

    return new Promise(function(resolve, reject) {
        

        console.info("Calling the Authentication Service")
        request({
            uri: envConfig.PDF_GEN_AUTH_ENDPOINT,
            method: 'POST',
            body: {
                accessKey: envConfig.PDF_GEN_ACCESS_KEY
            },
            json: true,
            resolveWithFullResponse: true 
        })
        .then(function (response) {
            console.log("Authenticated with Auth Endpoint. statusCode" + response.statusCode)
            
            console.info("Calling the RaaS PDF Generation Service")
            return request({
                uri: envConfig.PDF_GEN_API_ENDPOINT,
                method: 'POST',
                headers:{
                    "Authorization": response.body.token,
                    "Content-Type": "application/json"
                },
                body: reportData,
                json: true,
                resolveWithFullResponse: true 
            });
        })
        .then(function (response) {

            console.info("Generated PDF for report. statusCode" + response.statusCode)
    
            var s3 = new AWS.S3();
            var buf = new Buffer(response.body,'base64');
    
            console.info("Uploading PDF to S3 bucket " + process.env.TEMP_PDF_BUCKET)
            s3.upload({
                Bucket: process.env.TEMP_PDF_BUCKET,
                Body: buf,
                Key: Guid.raw(),
                ContentEncoding: 'base64',
                ContentType: 'application/pdf',
                ACL: 'public-read',
                region: 'us-east-1',
                CacheControl: 'no-cache',
                ContentDisposition: 'attachment; filename="' + filename + '"'
              }, function (err, data) {
    
                if(err){
                    console.log("Fatal Error :" + JSON.stringify(err));
                    reject(err);
                }
    
                console.info("Uploaded PDF to S3 bucket" + response.statusCode)
            
                console.info("Sending reponse to download PDF from S3 location");
                var dlresponse = {
                    statusCode: 307,
                    headers: {
                        "Location" : data.Location
                    },
                    body: null
                };

                resolve (dlresponse)
              });
    
        })
        .catch(function (err) {
            reject(err);
        });

    });

}



