'use strict';

var request = require('request-promise');
var _ = require('lodash')
var config = require('../config')
var reportGenerator = require('../utils/reportGenerator')

exports.handler = (event, context, callback) => {

    //Validate Inputs
    var jobDescriptionID = event.queryStringParameters.jobId 
    var authToken = event.queryStringParameters.authToken

    if(_.isNil(jobDescriptionID) || _.isNil(authToken)){
        console.error('Invalid Parameters');
        callback(new Error("Fatal Error : Invalid Parameters"));
        return;
    }
    console.log("Valid Inputs : JobID ",jobDescriptionID)

    //Validate Envrioment
    var KFPH_ENV = config.get(process.env.ENVIRONMENT);

    if(_.isNil(KFPH_ENV)){
        console.error('Invalid Environment');
        callback(new Error("Fatal Error : Invalid Environment"));
        return;
    }
    console.log("Environment: " + process.env.ENVIRONMENT)
    console.log(JSON.stringify(KFPH_ENV))

    //Report Data
    var reportData = {
        "instrument":{
            "reportVersion": "1",
            "ReportDefCode": "projects.internal.producthub.profilemanager.jobdescription.fullexport.workflow.pdf"
        },
        "static": {
            "reportInfo": {
            "reportTitle": "Job Description",
            "position": "FOR THE POSITION OF:",
            "copyright": "© Korn Ferry 2018. All rights reserved. Personal and Confidential"
            },
            "labels": {
                "kornferry" : "Korn Ferry"
            },
            "aboutKF": {
            "title": "About Korn Ferry",
            "desc1": "Korn Ferry is the preeminent global people and organizational advisory firm. We help leaders, organizations, and societies succeed by releasing the full power and potential of people. Our nearly 7,000 colleagues deliver services through Korn Ferry and our Hay Group and Futurestep divisions. Visit kornferry.com for more information.",
            "copyright": "© Korn Ferry 2018. All rights reserved. Personal and Confidential",
            "disclaimer": "For the sake of linguistic simplicity in this report, where the masculine form is used, the feminine form should also be understood to be included."
            }
        }
      };

    
    
      //Call API + Massage + Call Report Download
    console.info("Calling the Job Description API to retrieve data");
    request({
        rejectUnauthorized: false,
        uri: KFPH_ENV.KFPH_API_URL + '/v1/hrms/jobdescriptions/' + jobDescriptionID,
        method: 'GET',
        headers: {
            authToken: authToken
        },
        resolveWithFullResponse: true 
    })
    .then(function (response) {
        console.info("Retreived data from Job Description API. statusCode" + response.statusCode)

        reportData.data = JSON.parse(response.body).data

        var reportSections = reportData.data.sections;
        reportSections = _.reject(reportSections,{code: "ABOUT_THE_COMPANY"});
        reportSections = _.reject(reportSections,{code: "JOB_PURPOSE"});
        reportSections = _.reject(reportSections,{code: "ADDITIONAL_INFORMATION"});
        reportSections = _.reject(reportSections,{code: "WORK_CHARACTERISTICS"});
        reportSections = _.reject(reportSections,{code: "TASKS"});
        reportSections = _.reject(reportSections,{code: "TOOLS"});
        reportSections = _.reject(reportSections,{code: "TECHNOLOGY"});

        reportData.data.sections = reportSections;

        console.log("reportData" + JSON.stringify(reportData));

        return reportGenerator.downloadReport(KFPH_ENV, reportData, 'Job Description.pdf')
    })
    .then(function (response) {
        callback(null, response)
        context.done()
    })
    .catch(function (err) {
        callback("Fatal Error :" + JSON.stringify(err));
    });

}