'use strict';

var request = require('request-promise');
var _ = require('lodash')
var config = require('../config')
var reportGenerator = require('../utils/reportGenerator')

exports.handler = (event, context, callback) => {


    //Validate Inputs
    var spId = event.queryStringParameters.spId 
    var authToken = event.queryStringParameters.authToken

    if(_.isNil(spId) || _.isNil(authToken)){
        console.error('Invalid Parameters');
        callback(new Error("Fatal Error : Invalid Parameters"));
        return;
    }
    console.log("Valid Inputs : JobID ",spId)

    //Validate Envrioment
    var KFPH_ENV = config.get(process.env.ENVIRONMENT);

    if(_.isNil(KFPH_ENV)){
        console.error('Invalid Environment');
        callback(new Error("Fatal Error : Invalid Environment : " + process.env.ENVIRONMENT));
        return;
    }
    console.log("Environment: " + process.env.ENVIRONMENT)
    console.log(JSON.stringify(KFPH_ENV))

    var additionalAttributes = {
        "RESPONSIBILITY": {
            "label":"Responsiblity",
            "segments": [1,2,3,4,5,6,7,8,9,10,11,12,13],
            "order": 1
        },
        "WORK_CHARACTERISTICS": {
            "label":"Work Characteristic",
            "segments": [1,2,3],
            "order": 2
        },
        "BEHAVIORAL_SKILLS": {
            "label":"Behavioral Competency",
            "segments": [1,2,3,4,5,6,7,8],
            "order": 3
        },
        "TECHNICAL_SKILLS": {
            "label":"Technical Competency",
            "segments": [1,2,3,4,5],
            "order": 4
        },
        "EDUCATION": {
            "label":"Education",
            "segments": [1,2,3,4,5,6,7],
            "order": 5
        },
        "EXPERIENCE": {
            "label":"Experience",
            "segments": [1,2,3,4,5,6,7,8,9],
            "order": 6
        },
        "TRAITS": {
            "label":"Trait",
            "segments": [1,2,3,4,5,6,7,8,9,10],
            "order": 1
        },
       "DRIVERS": {
            "label":"Driver",
            "segments": [1,2,3,4,5,6,7,8,9,10],
            "order": 2
        }
    }
    //Report Data
    var reportData = {

        "instrument": {
            "reportVersion": "1",
            "ReportDefCode": "projects.internal.producthub.profilemanager.successprofile.fullexport.workflow.pdf"
        },
        "static": {
            "reportInfo": {
                "reportTitle": "SUCCESS PROFILE",
                "createdOn": "CREATED",
                "createdBy": "BY",
                "typeBIC": "Best-in-Class",
                "typeCustom": "Custom",
                "copyright": "© Korn Ferry 2018. All rights reserved. Personal and Confidential"
            },
            "aboutKF": {
                "title": "About Korn Ferry",
                "desc1": "Korn Ferry is the preeminent global people and organizational advisory firm. We help leaders, organizations, and societies succeed by releasing the full power and potential of people. Our nearly 7,000 colleagues deliver services through Korn Ferry and our Hay Group and Futurestep divisions. Visit kornferry.com for more information.",
                "copyright": "© Korn Ferry 2018. All rights reserved. Personal and Confidential",
                "disclaimer": "For the sake of linguistic simplicity in this report, where the masculine form is used, the feminine form should also be understood to be included."
            }
        }
      };

    
    
    //Call API + Massage + Call Report Download
    console.info("Calling the Success Profile API to retrieve data");
    request({
        rejectUnauthorized: false,
        uri: KFPH_ENV.KFPH_API_URL + '/v1/hrms/successprofiles/' + spId,
        method: 'GET',
        headers: {
            authToken: authToken
        },
        resolveWithFullResponse: true 
    })
    .then(function (response) {
        console.info("Retreived data from Job Description API. statusCode" + response.statusCode)

        //massage
        var spData = JSON.parse(response.body).data;

        //remove WORK_CHARACTERISTICS section
        spData.sections = _.reject(spData.sections,{code: "WORK_CHARACTERISTICS"});
        spData.sections = _.reject(spData.sections,{code: "TASKS"});
        spData.sections = _.reject(spData.sections,{code: "TOOLS"});
        spData.sections = _.reject(spData.sections,{code: "TECHNOLOGY"});

        //group by section groups
        var sectionGroups = []
        _.each(spData.sections, function(section){

            _.assign(section, additionalAttributes[section.code])

            var currSG = section.sectionGroup;

            var existingSG = _.find(sectionGroups, function(sg){return sg.id === currSG.id});
            if(_.isNil(existingSG)){
                var tempSG = _.clone(currSG)
                tempSG.sections = [];
                tempSG.sections.push(section);
                sectionGroups.push(_.clone(tempSG));
            }else{
                existingSG.sections.push(section);
            }

        });

        //sort each section in section group by order
        _.each(sectionGroups, function(sectionGroup){
            sectionGroup.sections = _.sortBy(sectionGroup.sections, function(section){return section.order});
        })

        spData.sectionGroups = sectionGroups;
        spData.sections = null;

        reportData.data = spData
        

        console.log("reportData" + JSON.stringify(reportData));

        return reportGenerator.downloadReport(KFPH_ENV, reportData, 'Success Profile.pdf')
    })
    .then(function (response) {
        callback(null, response)
        context.done()
    })
    .catch(function (err) {
        callback("Fatal Error :" + JSON.stringify(err));
    });

}