'use strict';

var _ = require('lodash')

const environments = {
    DEV: {
        KFPH_API_URL: 'https://testproductsapi.kornferry.com',
        PDF_GEN_AUTH_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/playground/authenticate',
        PDF_GEN_ACCESS_KEY: '5aa2b3cc166036abd803c7b64ed33f80c667b2d366bc6a86b73f0d2c622658f551ac0b23c07e11aef1c51cab641ac844fd7f904fff46d97ede85dc01c432e3d023c8ade3889d78375f9b',
        PDF_GEN_API_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/playground/secure/rptaas'
    },
    DEV_INT: {
        KFPH_API_URL: 'https://testproductsapi.kornferry.com',
        PDF_GEN_AUTH_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/dev/authenticate',
        PDF_GEN_ACCESS_KEY: '5aa2b3cc166036abd803c7b64ed33f80c667b2d366bc6a86b73f0d2c622658f551ac0b23c07e11aef1c51cab641ac844fd7f904fff46d97ede85dc01c432e3d023c8ade3889d78375f9b',
        PDF_GEN_API_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/dev/secure/rptaas'
    },
    TEST: {
        KFPH_API_URL: 'https://testproductsapi.kornferry.com',
        PDF_GEN_AUTH_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/qa/authenticate',
        PDF_GEN_ACCESS_KEY: '5aa2b3cc166036abd803c7b64ed33f80c667b2d366bc6a86b73f0d2c622658f551ac0b23c07e11aef1c51cab641ac844fd7f904fff46d97ede85dc01c432e3d023c8ade3889d78375f9b',
        PDF_GEN_API_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/qa/secure/rptaas'
    },
    STAGING: {
        KFPH_API_URL: 'https://stagingproductsapi.kornferry.com',
        PDF_GEN_AUTH_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/qa/authenticate',
        PDF_GEN_ACCESS_KEY: '5aa2b3cc166036abd803c7b64ed33f80c667b2d366bc6a86b73f0d2c622658f551ac0b23c07e11aef1c51cab641ac844fd7f904fff46d97ede85dc01c432e3d023c8ade3889d78375f9b',
        PDF_GEN_API_ENDPOINT: 'https://gcmauaj1d2.execute-api.us-east-1.amazonaws.com/qa/secure/rptaas'
    },
    PROD: {
        KFPH_API_URL: 'https://api.kornferry.com',
        PDF_GEN_AUTH_ENDPOINT: 'https://j735yqg24g.execute-api.us-east-1.amazonaws.com/prod/authenticate',
        PDF_GEN_ACCESS_KEY: '5aa2b3cc166036abd803c7b64ed33f80c667b2d366bc6a86b73f0d2c622658f551ac0b23927e16fbf1c713a4641ec34ca87f9019af13d925d9d184019830b3d5239effb585ca79615f9b',
        PDF_GEN_API_ENDPOINT: 'https://j735yqg24g.execute-api.us-east-1.amazonaws.com/prod/secure/rptaas'
    }
}

exports.get = (ph_env) => {

    if(_.isNil(ph_env)|| ph_env == ""){
        console.log("No envrionment provided. Using TEST");
        return environments["TEST"];;
    }

    return environments[ph_env];

}